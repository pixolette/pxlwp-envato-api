<?php declare(strict_types=1); # -*- coding: utf-8 -*-

namespace PxlwpEnvatoApi\Models;

/**
 * Interface Envato
 */
interface EnvatoInterface
{

    /**
     * Function to check purchase code
     *
     * @return string
     */
    public function getPurchaseCodeStatus(string $purchase_code, string $product_id): array;

}
