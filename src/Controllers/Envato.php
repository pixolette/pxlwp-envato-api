<?php declare(strict_types=1); # -*- coding: utf-8 -*-

namespace PxlwpEnvatoApi\Controllers;

use PxlwpEnvatoApi\Models\EnvatoInterface;

/**
 * class Envato
 *
 * @package PxlwpEnvatoApi\Controllers
 */
class Envato implements EnvatoInterface
{

    protected $code = [
        'valid' => 'valid',
        'invalid' => 'invalid',
    ];

    protected $status = [
        'valid' => 'valid',
        'invalid' => 'invalid',
        'expired' => 'expired',
        'different_id' => 'different_id',
    ];

    protected $api_url = 'https://api.envato.com/';
    protected $api_path_sale = 'v3/market/author/sale';

    /**
     * Function construct
     *
     * @param string $bearer_token to connect to Envato.
    */
    public function __construct(string $bearer_token)
    {
        $this->bearer_token = $bearer_token;
    }

    /**
     * Function getApiUrl
     *
     * @param string $action .
    */
    public function getApiUrl(string $action = ''): string
    {
        switch ($action) {
            case 'sale':
                $url = $this->api_url . $this->api_path_sale;
                break;

            default:
                $url = '';
                break;
        }

        return $url;
    }

    /**
     * Function getPurchaseCodeStatus
     *
     * @param string $purchase_code to be requested
     * @param string $product_id to be requested
    */
    public function getPurchaseCodeStatus(string $purchase_code, string $product_id): array
    {
        $curl_service = new \Ixudra\Curl\CurlService();
        $api_url = $this->getApiUrl('sale');
        $response = $curl_service->to($api_url)
            ->withHeader('Authorization: Bearer ' . $this->bearer_token)
            ->withData(['code' => $purchase_code])
            ->get();

        if ($response) {
            $response = json_decode($response, true);

            if (isset($response['error']) && isset( $response['reason'] ) && 'missing-token' == $response['reason']) {
                return [
                    'status' => $this->status['invalid'],
                    'code' => $this->status['invalid'],
                    'api-error' => true,
                ];
            } elseif (isset($response['error']) && 404 == $response['error']) {
                return [
                    'status' => $this->status['invalid'],
                    'code' => $this->status['invalid'],
                    'message' => $response['description'] ?? '',
                ];
            } elseif (isset($response['error'])) {
                return [
                    'status' => $this->status['invalid'],
                    'code' => $this->status['invalid'],
                    'message' => $response['error'] ?? ''
                ];
            } elseif (isset($response['license'])) {
                $expire_timestamp = isset($response['supported_until']) ? strtotime($response['supported_until']) : 0;
                $current_timestamp = time();
                if ($expire_timestamp < $current_timestamp) {
                    return [
                        'status' => $this->status['expired'],
                        'code' => $this->status['valid'],
                    ];
                } else {
                    if ($product_id != $response['item']['id']) {
                        return [
                            'status' => $this->status['different_id'],
                            'code' => $this->status['invalid'],
                        ];
                    } else {
                        return [
                            'status' => $this->status['valid'],
                            'code' => $this->status['valid'],
                        ];
                    }
                }
            }
        }

        return [
            'status' => $this->status['invalid'],
            'code' => $this->status['invalid'],
        ];
    }

    /**
     * Function getPurchaseCodeStatus
     *
     * @param string $purchase_code to be requested
     * @param string $product_id to be requested
    */
    public function getSaleDataByPurchaseCode(string $purchase_code): array
    {
        $response = array();
        $curl_service = new \Ixudra\Curl\CurlService();
        $api_url = $this->getApiUrl('sale');
        $response = $curl_service->to($api_url)
            ->withHeader('Authorization: Bearer ' . $this->bearer_token)
            ->withData(['code' => $purchase_code])
            ->get();

        if ($response) {
            $response = json_decode($response, true);
        }


        return $response;
    }
}
