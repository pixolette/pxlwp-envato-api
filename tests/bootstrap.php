<?php # -*- coding: utf-8 -*-

$root_directory = dirname(dirname(__DIR__));
$autoload_file  = $root_directory . '/vendor/autoload.php';

if (file_exists($autoload_file)) {
	require_once $autoload_file;

	return;
}
